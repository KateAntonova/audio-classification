import os
import shutil

import numpy as np
import tensorflow as tf
from tensorflow import keras

from pathlib import Path
from IPython.display import display, Audio

SAMPLING_RATE = 16000
SHUFFLE_SEED = 43
EPOCHS = 70
BATCH_SIZE = 128

HUMAN = "human"
SPOOF = "spoof"
CLASS_NAMES = [HUMAN, SPOOF]


def main():
    training_dataset_root = os.path.join(os.path.expanduser("~"), "Downloads/Training_Data")

    test_dataset_path = os.path.join(os.path.expanduser("~"), "Downloads/Testing_Data")
    dataset_human_path = os.path.join(training_dataset_root, HUMAN)
    dataset_spoof_path = os.path.join(training_dataset_root, SPOOF)

    # Get the list of all human and spoof files
    test_dataset = get_audios_from_dir(test_dataset_path)
    human_dataset = get_audios_from_dir(dataset_human_path)
    spoof_dataset = get_audios_from_dir(dataset_spoof_path)

    test_amount = len(test_dataset)
    human_amount = len(human_dataset)
    spoof_amount = len(spoof_dataset)
    print("Found {} human files and {} spoof files and {} test files"
          .format(human_amount, spoof_amount, test_amount))

    print("STEP 1. Start resample audio to 16000 Hz")
    i = 0
    all_parts = human_amount + spoof_amount + test_amount
    for file in human_dataset + spoof_dataset + test_dataset:
        i += 1
        if i % 500 == 0:
            print(i, all_parts)
        resample_audio(file)

    print("STEP 2. Create datasets")
    train_audio_paths = human_dataset + spoof_dataset
    train_labels = [0] * human_amount + [1] * spoof_amount

    rng = np.random.RandomState(SHUFFLE_SEED)
    rng.shuffle(train_audio_paths)
    rng = np.random.RandomState(SHUFFLE_SEED)
    rng.shuffle(train_labels)

    train_ds = paths_and_labels_to_dataset(train_audio_paths, train_labels)
    train_ds = train_ds.shuffle(buffer_size=BATCH_SIZE * 8, seed=SHUFFLE_SEED).batch(BATCH_SIZE)
    valid_ds = paths_and_labels_to_dataset(test_dataset, test_dataset)
    valid_ds = valid_ds.shuffle(buffer_size=BATCH_SIZE * 8, seed=SHUFFLE_SEED).batch(32)

    # Transform audio wave to the frequency domain using `audio_to_fft`
    train_ds = train_ds.map(lambda x, y: (audio_to_fft(x), y), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    train_ds = train_ds.prefetch(tf.data.experimental.AUTOTUNE)

    valid_ds = valid_ds.map(lambda x, y: (audio_to_fft(x), y), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    valid_ds = valid_ds.prefetch(tf.data.experimental.AUTOTUNE)

    print("STEP 3. Create and training model")
    model = build_model((SAMPLING_RATE // 2, 1), len(CLASS_NAMES))
    # model.summary()

    # Compile the model using Adam's default learning rate
    model.compile(
        optimizer="Adam", loss="sparse_categorical_crossentropy", metrics=["accuracy"]
    )

    # 'EarlyStopping' to stop training when the model is not enhancing anymore
    # 'ModelCheckPoint' to always keep the model that has the best val_accuracy
    model_save_filename = "model.h5"
    earlystopping_cb = keras.callbacks.EarlyStopping(patience=10, restore_best_weights=True)
    mdlcheckpoint_cb = keras.callbacks.ModelCheckpoint(
        model_save_filename, monitor="val_accuracy", save_best_only=True
    )

    model.fit(
        train_ds,
        epochs=EPOCHS,
        callbacks=[earlystopping_cb, mdlcheckpoint_cb],
    )
    model.save("audio model")
    # model = keras.models.load_model("audio model")

    print('STEP 4. Demonstration')
    file = open("results.txt", "w+")

    chanks = len(valid_ds)
    for audios, labels in valid_ds.take(chanks):
        predict = model.predict(audios)

        for i in range(len(predict)):
            full_file_name = labels[i].numpy().decode("utf-8")
            pos_of_name = full_file_name.find('sample_')
            file_name = full_file_name[pos_of_name:]

            file.write('{}, {:.12f}\n'.format(file_name, predict[i][0]))

    file.close()


def residual_block(x, filters, conv_num=3, activation="relu"):
    s = keras.layers.Conv1D(filters, 1, padding="same")(x)
    for i in range(conv_num - 1):
        x = keras.layers.Conv1D(filters, 3, padding="same")(x)
        x = keras.layers.Activation(activation)(x)
    x = keras.layers.Conv1D(filters, 3, padding="same")(x)
    x = keras.layers.Add()([x, s])
    x = keras.layers.Activation(activation)(x)
    return keras.layers.MaxPool1D(pool_size=2, strides=2)(x)


def build_model(input_shape, num_classes):
    inputs = keras.layers.Input(shape=input_shape, name="input")

    x = residual_block(inputs, 16, 2)
    x = residual_block(x, 32, 2)
    x = residual_block(x, 64, 3)
    x = residual_block(x, 128, 3)
    x = residual_block(x, 128, 3)

    x = keras.layers.AveragePooling1D(pool_size=3, strides=3)(x)
    x = keras.layers.Flatten()(x)
    x = keras.layers.Dense(256, activation="relu")(x)
    x = keras.layers.Dense(128, activation="relu")(x)

    outputs = keras.layers.Dense(num_classes, activation="softmax", name="output")(x)
    return keras.models.Model(inputs=inputs, outputs=outputs)


def get_audios_from_dir(dir_path):
    files = []
    if os.path.isdir(dir_path):
        files += [
            os.path.join(dir_path, filepath)
            for filepath in os.listdir(dir_path)
            if filepath.endswith(".wav")
        ]
    return files


# Resample all noise samples to 16000 Hz
def resample_audio(file):
    command = (
            "sample_rate=`ffprobe -hide_banner -loglevel panic -show_streams "
            + file + " | grep sample_rate | cut -f2 -d=`; "
                     "if [ $sample_rate -ne 16000 ]; then "
                     "ffmpeg -hide_banner -loglevel panic -y "
                     "-i $file -ar 16000 temp.wav; "
                     "mv temp.wav $file; "
                     "fi"
    )
    os.system(command)


def paths_and_labels_to_dataset(audio_paths, labels):
    """Constructs a dataset of audios and labels."""
    path_ds = tf.data.Dataset.from_tensor_slices(audio_paths)
    audio_ds = path_ds.map(lambda x: path_to_audio(x))
    label_ds = tf.data.Dataset.from_tensor_slices(labels)
    return tf.data.Dataset.zip((audio_ds, label_ds))


def path_to_audio(path):
    """Reads and decodes an audio file."""
    audio = tf.io.read_file(path)
    audio, _ = tf.audio.decode_wav(audio, 1, SAMPLING_RATE)
    return audio


def audio_to_fft(audio):
    # Since tf.signal.fft applies FFT on the innermost dimension,
    # we need to squeeze the dimensions and then expand them again
    # after FFT
    audio = tf.squeeze(audio, axis=-1)
    fft = tf.signal.fft(
        tf.cast(tf.complex(real=audio, imag=tf.zeros_like(audio)), tf.complex64)
    )
    fft = tf.expand_dims(fft, axis=-1)

    # Return the absolute value of the first half of the FFT
    # which represents the positive frequencies
    return tf.math.abs(fft[:, : (audio.shape[1] // 2), :])


if __name__ == '__main__':
    main()
